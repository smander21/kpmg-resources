﻿function extractZip( $source, $target )
{
    Write-Host "extracting: " + $source;
    [System.IO.Compression.ZipFile]::ExtractToDirectory( $source, $target );
}

$sourceFile = $args[0];
$targetFile = $args[1];

Add-Type -AssemblyName System.IO.Compression.FileSystem;

extractZip $sourceFile $targetFile;
